//
//  ViewController.h
//  Storytime
//
//  Created by Azizi on 20/03/2019.
//  Copyright © 2019 Aleksander Azizi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SafariServices/SafariServices.h> // In-app browser
#import <AVKit/AVKit.h>

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate> {
    NSDictionary *jsonData;
    
    NSMutableArray *entries;
    UITableView *entriesTableView;
    UIRefreshControl *refreshControl;
    
    BOOL search;
    UISearchBar *searchBar;
    NSMutableArray *searchEntries;
}


@end
