//
//  ViewController.m
//  Storytime
//
//  Created by Azizi on 20/03/2019.
//  Copyright © 2019 Aleksander Azizi. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // UI
    [self createMainView];

    entries = [[NSMutableArray alloc] init];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://0.0.0.0:8085"]]; // https://pastebin.com/raw/4VE4MiaF
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        self->jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        
        for (NSString *key in self->jsonData) {
            NSLog(@"%@", key);
            [self->entries addObject:key];
        }
        
        [self refreshEntries];
    }] resume];
}

- (void)refreshEntries {
    // Refresh data
    [refreshControl beginRefreshing];
    [entriesTableView setContentOffset:CGPointMake(0, -refreshControl.frame.size.height -entriesTableView.tableHeaderView.frame.size.height) animated:true];
    
    //[rss initWithURL:[self preferencesForKey:@"url"] completion:^(NSArray *data) {
        //self->entries = data;
        [self->entriesTableView reloadData];
        [self->refreshControl endRefreshing];
   // }];
}


- (void)createMainView {
    CGSize mainViewSize = self.view.frame.size;
    [self.view setBackgroundColor:[UIColor colorWithRed:25.0/255.0 green:25.0/255.0 blue:25.0/255.0 alpha:1.0]];
    
    // Table view
    CGRect tableViewRect = CGRectMake(0, 0, mainViewSize.width, mainViewSize.height);
    entriesTableView = [[UITableView alloc] initWithFrame:tableViewRect style:UITableViewStylePlain];
    [entriesTableView setBackgroundColor:self.view.backgroundColor];
    [entriesTableView setSeparatorColor:[UIColor whiteColor]];
    
    [entriesTableView setDelegate:self];
    [entriesTableView setDataSource:self];
    
    
    // Table view refresh control
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl setTintColor:[UIColor whiteColor]];
    [refreshControl addTarget:self action:@selector(refreshViewPulled:) forControlEvents:UIControlEventValueChanged];
    [entriesTableView setRefreshControl:refreshControl];
    
    
    // Table view header
    CGSize headerViewSize = CGSizeMake(mainViewSize.width, 44);
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, headerViewSize.width, headerViewSize.height)];
    
    // Search bar
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, headerViewSize.width - headerViewSize.height, 44)];
    [searchBar setBarTintColor:entriesTableView.backgroundColor];
    [searchBar setDelegate:self];
    [headerView addSubview:searchBar];
    
    // Preferences button
    UIButton *prefButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [prefButton setFrame:CGRectMake(headerViewSize.width - headerViewSize.height, 8, headerViewSize.height - 16, headerViewSize.height - 16)];
    [prefButton setImage:[UIImage imageNamed:@"prefs"] forState:UIControlStateNormal];
    [prefButton setTintColor:[UIColor whiteColor]];
    [prefButton addTarget:self action:@selector(createPreferencesView) forControlEvents:UIControlEventTouchUpInside];
    //[headerView addSubview:prefButton];
    
    [entriesTableView setTableHeaderView:headerView];
    
    
    [self.view addSubview:entriesTableView];
}

- (void)refreshViewPulled:(UIRefreshControl *)refreshControl {
    [self refreshEntries];
}

// Table View

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (search) {
        return searchEntries.count;
    } else {
        return entries.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"transactionsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        [cell.textLabel setTextColor:[UIColor whiteColor]];
        [cell.detailTextLabel setTextColor:[UIColor whiteColor]];
        [cell setBackgroundColor:tableView.backgroundColor];
        NSString *text, *detailText;
        
        if (search) {
            text = [searchEntries objectAtIndex:indexPath.row];
            detailText = @"";
        } else {
            NSString *tittel, *emne, *foreleser;
            tittel = [[jsonData objectForKey:[entries objectAtIndex:indexPath.row]] objectForKey:@"Tittel"];
            emne = [[jsonData objectForKey:[entries objectAtIndex:indexPath.row]] objectForKey:@"Emne"];
            foreleser = [[jsonData objectForKey:[entries objectAtIndex:indexPath.row]] objectForKey:@"Foreleser"];
            text =  tittel;
            detailText = [NSString stringWithFormat:@"%@ - %@", emne, foreleser];
        }
        
        [cell.textLabel setText:text];
        [cell.detailTextLabel setText:detailText];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *hash = [entries objectAtIndex:indexPath.row];
    NSString *url = [NSString stringWithFormat:@"https://forelesning.gjovik.ntnu.no/publish/%@/camera.mp4", hash];
    
    NSURL *videoURL = [NSURL URLWithString:url]; // https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4
    AVPlayer *player = [AVPlayer playerWithURL:videoURL];
    AVPlayerViewController *playerViewController = [AVPlayerViewController new];
    playerViewController.player = player;
    [self.view addSubview:playerViewController.view];
    [self presentViewController:playerViewController animated:YES completion:nil];
    
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}


// Search bar

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (searchText.length == 0) {
        search = false;
        [searchBar endEditing:true];
    } else {
        search = true;
        searchEntries = [[NSMutableArray alloc] init];
        
        for (NSArray *entry in entries) {
            NSString *entryTitle = [entry valueForKey:@"title"];
            NSRange range = [entryTitle rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if (range.location != NSNotFound) {
                [searchEntries addObject:entryTitle];
            }
        }
    }
    
    [entriesTableView reloadData];
}

@end
