package com.example.lecture;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class JsonHandler extends AsyncTask<Void, Integer, String> {
    protected String doInBackground(Void...arg0) {
        try  {

            URL url = new URL("http://172.20.10.2:8085");
            URLConnection request = url.openConnection();
            request.connect();
            JsonParser jp = new JsonParser(); //from gson
            JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent())); //Convert the input stream to a json element
            JsonObject rootobj = root.getAsJsonObject(); //May be an array, may be an object.


            Singleton singleton = Singleton.getInstance();
            singleton.setData(rootobj);
        } catch(Exception e) {
            Log.e("MYAPP", "exception: " + e.getMessage());
            Log.e("MYAPP", "exception: " + e.toString());
        }
        return "Errooor";
    }
}