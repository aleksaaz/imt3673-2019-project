package com.example.lecture;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MainActivity extends Activity {
    int count = 0;
    String topic;
    String mTopic[];
    String lecturer;
    String mLecturer[];
    String date;
    ArrayList<String> keys;
    int image = R.drawable.videoimage;
    ListView videoList;


    private void openVideoWithUrl(String url) {
        Intent myIntent = new Intent(MainActivity.this, VideoActivity.class);
        myIntent.putExtra("url", url); //Optional parameters
        MainActivity.this.startActivity(myIntent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get reference of widgets from XML layout
        videoList = findViewById(R.id.videoList);
        Button btn = (Button) findViewById(R.id.btn);

        // Initializing a new String Array
        String[] videos = new String[] {

        };



        // Create a List from String Array elements
        final List<String> video_list = new ArrayList<String>(Arrays.asList(videos));

        // Create an ArrayAdapter from List
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, video_list);


        // DataBind ListView with items from ArrayAdapter
        videoList.setAdapter(arrayAdapter);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new JsonHandler().execute();

                arrayAdapter.notifyDataSetChanged();
                keys = new ArrayList<String>();
                Singleton singleton = Singleton.getInstance();
                if(singleton.status()){
                    JsonObject jsonObject = singleton.getData();

                    for (String key: jsonObject.getAsJsonObject().keySet()) {

                        keys.add(key);
                        JsonObject rootobj = jsonObject.get(key).getAsJsonObject();

                        lecturer = rootobj.get("Foreleser").getAsString(); // grab the lecturer
                        //mLecturer[count] = rootobj.get("lecturer").getAsString(); // grab the lecturer

                        topic = rootobj.get("Emne").getAsString(); // grab topic
                        //mTopic[count] = rootobj.get("topic").getAsString(); // grab topic

                        date = rootobj.get("Tittel").getAsString(); // grab the date
                        //mDate[count] = date = rootobj.get("date").getAsString(); // grab the date


                        //mImages[count] =  R.drawable.videoimage;
                        video_list.add(topic + " "+ lecturer + " " + date);
                        count++;
                    }
                }

            }
        });

        videoList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final String hash;
                Log.d("onItemClick: ",keys.get(position));
                openVideoWithUrl("http://forelesning.gjovik.ntnu.no/publish/"+ keys.get(position) + "/camera.mp4");
            }
        });



    }

    class  MyAdapter extends ArrayAdapter<String>{
        Context context;
        String rTopic;
        String rLecturer;
        String rDate;
        int rImgs;

        MyAdapter (Context c, String rTopic, String rLecturer, String rDate/*, int rImgs[]*/){
            super(c, R.layout.row, R.id.textView1, Collections.singletonList(rTopic));
            this.context = c;
            this.rTopic = topic;
            this.rLecturer = lecturer;
            this.rDate = date;
            this.rImgs = image;
        }
        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.row,parent, false);
            ImageView images = row.findViewById(R.id.image);
            TextView myTitle = row.findViewById(R.id.textView1);
            TextView myDescription = row.findViewById(R.id.textView2);


            //set proper values

            images.setImageResource(rImgs);
            myTitle.setText(rImgs);
            myDescription.setText(rTopic);
            return super.getView(position, convertView, parent);

        }
    }
}





