package com.example.lecture;

import com.google.gson.JsonObject;

public class Singleton {
    private static final Singleton ourInstance = new Singleton();
    private JsonObject data;

    public static Singleton getInstance() {
        return ourInstance;
    }

    private Singleton() {
    }

    public void setData(JsonObject jsonObject) {
        data = jsonObject;
    }

    public JsonObject getData() {
        return data;
    }

    public boolean status() {
        if (data == null) return false;
        return true;
    }
}
