//
//  Copyright © 2019 Aleksander Azizi. All rights reserved.
//

package main

import (
	"log"
	"strings"
	"net/http"
	"encoding/json"
	"github.com/PuerkitoBio/goquery"
)

var lectures map[string]interface{}

func main() {
	// Request, scrape, and generate JSON
	document := RequestDocument("https://forelesning.gjovik.ntnu.no/publish/")
	lectures = GenerateJSON(document)

	// Callback API
	Serve()
}

func RequestDocument(url string) (*goquery.Document) {
	log.Println("Requesting " + url);

	// GET Request
	response, err := http.Get(url)
	if err != nil { log.Fatal(err) }
	defer response.Body.Close()


	log.Println("Scraping document");

	// Create goquery document from HTTP response
	document, err := goquery.NewDocumentFromReader(response.Body)
	if err != nil { log.Fatal("Error loading HTTP response body. ", err) }

	return document
}

func GenerateJSON(document *goquery.Document) map[string]interface{} {
	log.Println("Generating json");
	
	// Init JSON object
	jsonObject := map[string]interface{}{}
	keys := []string{}

	// Lectures table
	lecturesTable := document.Find("table[id=lectures]")

	// Get meta keys
	lecturesTable.Find("tr:not([class=lecture])").Each(func(index int, element *goquery.Selection) {
		element.Find("th").Each(func(indexy int, elementy *goquery.Selection) {
			text := elementy.Find("a").Text()
			if (len(text) > 0) {
				keys = append(keys, text)
			}
		})
	})

	// Get values
	lecturesTable.Find("tr[class=lecture]").Each(func(index int, element *goquery.Selection) {
		values := []string{}

		element.Find("td").Each(func(indexy int, elementy *goquery.Selection) {
			// Get a href with link to video
			link, exists := elementy.Find("a").Attr("href")

			// If link is found (end of table cell)
			if exists {
				// Get hash from url
				hash := strings.Split(link, "/")[0]

				// Create the JSON object
				lectureObject := map[string]interface{}{}

				for key := 0; key < len(keys); key++ {
					lectureObject[keys[key]] = values[key]
				}

				jsonObject[hash] = lectureObject
			} else {
				// Append to values
				text := elementy.Text()
				values = append(values, text)
			}
		})

		// Clear values
		values = []string{}
	})

	return jsonObject
}

func Serve(){
	log.Println("Serving JSON on :8085");
	http.HandleFunc("/", Routes)
	http.ListenAndServe(":8085", nil) //os.Getenv("PORT")
}

// Routes requests
func Routes(response http.ResponseWriter, request *http.Request) {
	log.Println(request.Method, request.URL.Path) // Print request

	// Encode JSON
	bytesRepresentation, err := json.Marshal(lectures)
	if err != nil { log.Fatalln(err) } else {
		// Write
		response.Header().Set("Content-Type", "application/json")
		response.Header().Set("Access-Control-Allow-Origin", "*")
		response.Write([]byte(bytesRepresentation))
	}
}