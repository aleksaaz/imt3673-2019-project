# Lecture

Lecture apps for iOS and Android.

•	iOS: Tested with Xcode v10.2 using (iPhone XS Max) Simulator running iOS 12.

•	Android: Tested with Android Studio v3.4 using (Nexus 5X API 28) Emulator running Android 9.

•	Backend: Tested with Go v1.11 (darwin/amd64).


## Project focus

•	Developing a solution-stack (utilizing existing database) for streaming NTNU Gjøvik lectures.

•	Project focus on full-stack mobile development with multiple platforms (learning experience).

•	Generalized by writing backend in Go for scraping and generating application data.
